# Setup Information
1. Install Python version 3.6 or above if you haven't already.
2. Install the dependencies with pip:
```sh
pip install -u websockets
```
or with pipenv
```sh
pip install pipenv # if you don't have pipenv already installed
pipenv install
```
3. Run the file `bot.py` with:
```sh
python bot.py
```
or with pipenv
```sh
pipenv run bot.py
```
4. Create a new pomf.tv account for your bot. Don't use your own.
5. Navigate to the url https://pomf.tv/help#chatoverlay **after** logging into
   your bot account on **pomf**. You will need the chat url.
6. Follow the instructions to setup the bot's login information from the chat
   url and then type in your regular username. **Just** your regular username
   the bot will connect to your stream chat after connecting.

# How do I add new commands?
*For now* you have to add commands manually, but they are quite easy to do
1. open the file `bot.py` and search for the line
```python
# Add new commands down here
```
2. Edit the following template:
```python
elif "!COMMAND" in inbound_message.message:
    await self.send_message("RESPONSE")
```
* Change the text `!COMMAND` to the command you'd like the bot to respond.
* Change the text `RESPONSE` on the second line of the template to setup the
  answer the bot should give
3. Add the command from the template under the text add new commands

   PS: be careful with the identation, the command needs to follow it
   after all, the commands are nothing but ifs and elses!
4. Edit the help command by seaching the line
```python
# This is the help command, edit it's message to change it
```
  to modify the bot's help

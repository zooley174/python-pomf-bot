import asyncio
import json
import datetime
import tracemalloc

from dataclasses import dataclass
from pathlib import Path
from urllib import parse

import websockets


tracemalloc.start()

uri = "wss://pomf.tv/websocket/"
pomf_help =  "https://pomf.tv/help"
chat_link = "https://pomf.tv/help#chatoverlay"

bot_login_path = Path("bot_login_info.json")

try:
    with bot_login_path.open() as login_info:
        parsed_info = json.load(login_info)
except FileNotFoundError:
    print("Please create a NEW account for your bot for security reasons")
    print("!!! Attention !!!")
    print("NEVER EVER share your api code with anyone.")
    print("NO ONE.")
    print()
    print("This setup will generate a file called 'bot_login_info.json'")
    print("It contains information of your bot account.")
    print("DO NOT SHARE IT.")
    print()
    print(f"Go into the URL {pomf_help} after creating your bot account")
    print(f"Go down to the 'Chat URL for OBS/Xsplit' or this link {chat_link}")
    print("And copy and then paste the URL in here. Then press enter.")

    chat_url = input("Chat URL: ")
    room_name = input("Type your regular pomf username: ")

    print("After connecting we will automatically enter your stream room")
    print()

    url_query = parse.parse_qs(parse.urlparse(chat_url).query)
    parsed_info = {
        "channel_to_connect": room_name,
        "pomf_api": url_query["apikey"][0],
        "user_name": url_query["stm"][0],
    }

    with bot_login_path.open("w") as login_info:
        json.dump(parsed_info, login_info)

    print("Bot setup finished. To change anything, close the bot then delete")
    print("the file 'bot_login_info.json' and run the bot again")

channel_to_connect = parsed_info["channel_to_connect"]
pomf_api = parsed_info["pomf_api"]
user_name = parsed_info["user_name"]


@dataclass
class Message:
    message_type: str
    message_from: str
    timestamp: int
    styledname: str
    roomid: str
    avatar: str
    message: str
    msgid: int

    def __post_init__(self):
        self.message_from = self.message_from["name"]
        self.timestamp = datetime.datetime.fromtimestamp(self.timestamp)


class Bot:
    async def send_raw_message(self, msg_dict):
        return await self.websocket.send(json.dumps(msg_dict))

    async def connect_to_chat(self):
        conn_dict = {
            "roomId": channel_to_connect,
            "apikey": pomf_api,
            "userName": user_name,
            "action": "connect"
        }

        return await self.send_raw_message(conn_dict)

    async def send_message(self, msg):
        msg_dict = {
            "message": msg,
            "action": "message",
        }

        return await self.send_raw_message(msg_dict)

    async def run(self):
        print("Attempting to connect to pomf, pomf, pomf.tv")
        async with websockets.connect(uri) as websocket:
            self.websocket = websocket
            await self.connect_to_chat()

            print(f"Bot with username {user_name}")
            print(f"Connected into the chat of {channel_to_connect}")

            while True:
                raw_response = await websocket.recv()

                msg_dict = json.loads(raw_response)

                # renaming to avoid imcompatibilities with python keywords
                msg_dict["message_from"] = msg_dict["from"]
                msg_dict["message_type"] = msg_dict["type"]

                del msg_dict["type"]
                del msg_dict["from"]

                inbound_message = Message(**msg_dict)

                # This is the help command, edit it's message to change it
                if "!help" in inbound_message.message:
                    await self.send_message(
                        "This bot does nothing yet, carry on!"
                    )

                # Add new commands down here


bot = Bot()
asyncio.run(bot.run())
